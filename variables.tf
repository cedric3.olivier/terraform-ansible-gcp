variable "gcp_project" {
  type        = string
  default     = "test-terraform-sr"
  description = "The GCP project to deploy the runner into."
}
variable "gcp_zone" {
  type        = string
  default     = "europe-west1-b"
  description = "The GCP zone to deploy the runner into."
}

variable "gcp_region" {
  type        = string
  default     = "europe-west1"
  description = "The GCP region to deploy the runner into."
}
variable "ci_runner_instance_type" {
  type        = string
  default     = "e2-micro"
}
variable "hostname" {
  type        = string
  default     = "gitlab-runner.example.com"
}
variable "cle_publique" {
  type        = string
}
