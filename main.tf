terraform {
  backend "http" {}
}

// Configure the Google Cloud provider
provider "google" {
  project     = var.gcp_project
  region      = var.gcp_region
  zone        = var.gcp_zone
}

resource "google_compute_network" "vpc_network" {
  name = "vpc-network"
}

resource "google_compute_instance" "gitalb-ci-runner" {
  name                      = "gitlab-ci-runner"
  hostname                  = var.hostname
  machine_type              = var.ci_runner_instance_type
  project                   = var.gcp_project
  zone                      = var.gcp_zone

  scheduling {
    preemptible       = true
    automatic_restart = false
  }

  boot_disk {
    initialize_params {
      image = "rocky-linux-cloud/rocky-linux-8"
      size  = 20
      type  = "pd-standard"
    }
  }
  labels = {
    environment = "dev"
  }
  metadata = {
    enable-oslogin = "TRUE"
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
      // Include this section to give the VM an external ip address
    }
  }
}

resource "google_service_account" "service_account" {
  account_id   = "terraform"
  display_name = "terraform"
}
resource "google_service_account_key" "service_account" {
  service_account_id = google_service_account.service_account.name
  public_key_type    = "TYPE_X509_PEM_FILE"
}
resource "local_file" "service_account" {
    content  = base64decode(google_service_account_key.service_account.private_key)
    filename = "./service_account.json"
}
/* resource "google_project_iam_binding" "project" {
  project = var.gcp_project
  role    = "roles/viewer"

  members = [
    "serviceAccount:${google_service_account.service_account.email}",
  ]
} */
output "ip" {
  value = google_compute_instance.gitalb-ci-runner.network_interface.0.access_config.0.nat_ip
}

